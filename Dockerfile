FROM ubuntu

MAINTAINER Anthony Roscio "@roscioa"

RUN apt-get update && apt-get -y -q install vim python3 sudo curl python3-pip unzip nano locate

COPY enableMongo.sh .

RUN pip3 install --upgrade pip

RUN sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10

RUN sudo echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' >> /etc/apt/sources.list.d/mongodb.list

RUN sudo echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' >> /etc/apt/sources.list

RUN sudo apt-get update

RUN mkdir -p /data/db

RUN apt-get install mongodb-10gen=2.2.3

RUN echo "mongodb-10gen hold" | sudo dpkg --set-selections

RUN mkdir -p /usr/src/cvesearch

RUN curl -SL https://github.com/cve-search/cve-search/archive/master.zip --output /usr/src/cvesearch/master.zip

RUN unzip /usr/src/cvesearch/master.zip -d /usr/src/cvesearch

RUN sudo pip3 install -r /usr/src/cvesearch/cve-search-master/requirements.txt

RUN sudo chmod 755 ./enableMongo.sh

RUN sudo ./enableMongo.sh

#RUN mongod -v &

#RUN python3 /usr/src/cvesearch/cve-search-master/sbin/db_mgmt.py -p

#RUN python3 /usr/src/cvesearch/cve-search-master/sbin/db_mgmt_cpe_dictionary.py

#RUN python3 /usr/src/cvesearch/cve-search-master/sbin/db_updater.py -c
